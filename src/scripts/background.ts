import images from '../images/hex/*.png';
import BackgroundAnimation from "./BackgroundAnimation";

let urls: Array<string> = [];

Object.keys(images).forEach( (test: string) => urls.push(images[test]));

console.log("Available Images: ", urls);

new BackgroundAnimation(256, 256, urls);